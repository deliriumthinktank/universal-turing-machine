library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package turing_machine_specifications is
    type states_type is (a, b, c, d, e, stop);

    type symbols_type is range 0 to 1;

    type actions_type is (left, right, stay);

    type rule_type is record
        state  : states_type;
        action : actions_type;
        symbol : symbols_type;
    end record;

    type rules_type is array (states_type range <>, symbols_type range <>) of rule_type;

    type tape_type is array (natural range <>) of symbols_type;

    type machine_type is record
        initial_state : states_type;
        halt_state    : states_type;
        blank_symbol  : symbols_type;
    end record;
end turing_machine_specifications;
