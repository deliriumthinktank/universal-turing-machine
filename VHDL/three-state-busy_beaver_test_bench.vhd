library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.turing_machine_specifications.all;

entity three_state_busy_beaver_test_bench is
end entity;

architecture test_bench of three_state_busy_beaver_test_bench is
    constant clock_period : time := 1000 ms / 100;

    constant rules : rules_type := (a => (0 => (b, right, 1),
                                          1 => (c, left, 1)),
                                    b => (0 => (a, left, 1),
                                          1 => (b, right, 1)),
                                    c => (0 => (b, left, 1),
                                          1 => (stop, stay, 1)));

    constant machine : machine_type := (
        initial_state => a,
        halt_state    => stop,
        blank_symbol  => 0
    );

    signal data_in : tape_type(0 to 5);

    signal data_out : tape_type(0 to 5);

    signal clock : std_logic := '1';

    signal reset : std_logic := '0';

    signal done : std_logic := '0';
begin
    three_state_busy_beaver : entity work.turing_machine(machine)
        port map(
            clock    => clock,
            reset    => reset,
            done     => done,
            data_in  => data_in,
            data_out => data_out,
            rules    => rules,
            machine  => machine
        );

    clock <= not clock after clock_period / 2 when done /= '1' else '0';

    process is
    begin
        wait until done'event;

        report "done";

        for i in data_out'range loop
            report symbols_type'image (data_out(i));
        end loop;

        wait;
    end process;
end architecture;
