# Universal Turing Machine in VHDL 2008

The implementation of the universal turing machine in VHDL 2008. The machine itself is implemented in `turing_machine.vhd` and there are 3 examples that implement the tasks presented by [Rosetta Code](https://rosettacode.org/wiki/Universal_Turing_machine).

## Notes on simulation and synthesis

During the research on doing this implementation I found that VHDL 2019 would have brought major benefits to a cleaner design with the improvements in generic constraints. Since many of the major simulator doesn't support VHDL 2019 (and barely VHDL 2008) I choose not to go with VHDL 2019, so I could at least simulate the implementation - and hopefully synthesis the design on a FPGA in the future.

This has been simulated on GHDL 1.0-dev () [Dunoon edition] (Compiled with GNAT Version: 10.1.1 20200507)

This has not been synthesized yet, but steps has been taken to at least make the code synthesizable in theory.
