library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.turing_machine_specifications.all;

entity turing_machine is
    port(
        clock    : in  std_logic;
        reset    : in  std_logic;
        done     : out std_logic;
        data_in  : in  tape_type;
        data_out : out tape_type;
        rules    : in  rules_type;
        machine  : in  machine_type
    );
end entity turing_machine;

architecture machine of turing_machine is
begin
    process(all) is
        variable state : states_type := machine.initial_state;

        variable symbol : symbols_type;

        variable rule : rule_type;

        variable index : integer := 0;

        variable internal_register : tape_type(data_in'range) := data_in;
    begin
        if reset = '1' then
            state := machine.initial_state;

            index := 0;
        elsif state = machine.halt_state then
            done <= '1';
        elsif rising_edge(clock) then
            symbol := internal_register(index);

            rule := rules(state, symbol);

            internal_register(index) := rule.symbol;

            case rule.action is
                when left =>
                    if index = 0 then
                        internal_register := machine.blank_symbol & internal_register(0 to internal_register'length - 2);
                    else
                        index := index - 1;
                    end if;
                when right =>
                    index := index + 1;
                when stay => null;
            end case;

            state := rule.state;

            data_out <= internal_register;
        end if;
    end process;
end architecture machine;
