#include "generic_tape.h"
#include "generic_turing_machine.h"

using namespace Universal_Turing_Machine;

int sc_main(int argc, char *argv[]) {
    typedef enum { A, B, C, D, E, H } States;

    typedef int Symbols;

    typedef enum { Left, Stay, Right } Actions;

    int tape_size = 12289;

    using Specific_Turing_Machine = Generic_Turing_Machine<States, Symbols, Actions>;

    Specific_Turing_Machine::Rules rules = {
            {A, {{0, {1, Right, B}}, {1, {1, Left, C}}}},
            {B, {{0, {1, Right, C}}, {1, {1, Right, B}}}},
            {C, {{0, {1, Right, D}}, {1, {0, Left, E}}}},
            {D, {{0, {1, Left, A}}, {1, {1, Left, D}}}},
            {E, {{0, {1, Stay, H}}, {1, {0, Left, A}}}},
    };

    Specific_Turing_Machine specific_turing_machine(
            "machine", A, H, 0, Left, Stay, Right, rules);

    using Specific_Tape = Generic_Tape<Symbols>;

    Specific_Tape specific_tape("specific_tape", tape_size);

    sc_clock clock_signal;

    sc_signal<Symbols> data_in_out_signal;

    sc_signal<Symbols> data_out_in_signal;

    sc_signal<int> address_signal;

    sc_buffer<Tape_Statuses> status_signal;

    sc_buffer<Tape_Instructions> instruction_signal;

    specific_tape.clock_port(clock_signal);
    specific_tape.address_port(address_signal);
    specific_tape.data_in_port(data_in_out_signal);
    specific_tape.data_out_port(data_out_in_signal);
    specific_tape.status_port(status_signal);
    specific_tape.instruction_port(instruction_signal);

    specific_turing_machine.clock_port(clock_signal);
    specific_turing_machine.tape_address_port(address_signal);
    specific_turing_machine.tape_data_in_port(data_out_in_signal);
    specific_turing_machine.tape_data_out_port(data_in_out_signal);
    specific_turing_machine.tape_status_port(status_signal);
    specific_turing_machine.tape_instruction_port(instruction_signal);

    sc_start();

    for (int i = 0; i < tape_size; i++) {
        cout << specific_tape.buffer[i];
    }

    return 0;
}