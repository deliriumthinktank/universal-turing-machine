#ifndef SYSTEMC_INTERFACE_TAPE_H
#define SYSTEMC_INTERFACE_TAPE_H

namespace Universal_Turing_Machine {
    typedef enum { Read, Write, Shift } Tape_Instructions;

    typedef enum { Read_Done, Write_Done, Shift_Done } Tape_Statuses;
} // namespace Universal_Turing_Machine

#endif // SYSTEMC_INTERFACE_TAPE_H