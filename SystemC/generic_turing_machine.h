#ifndef SYSTEMC_GENERIC_TURING_MACHINE_H
#define SYSTEMC_GENERIC_TURING_MACHINE_H

#include "interface_tape.h"
#include <map>
#include <systemc.h>

namespace Universal_Turing_Machine {
    template<typename States, typename Symbols, typename Actions> class Generic_Turing_Machine : sc_module {
        SC_HAS_PROCESS(Generic_Turing_Machine);

    public:
        typedef struct {
            Symbols symbol;
            Actions action;
            States state;
        } Rule;

        typedef std::map<States, std::map<Symbols, Rule>> Rules;

    private:
        typedef enum {
            Read_Memory,
            Write_Memory,
            Shift_Memory,
            Finalize
        } Process_States;

    public:
        sc_in_clk clock_port;

        sc_out<Tape_Instructions> tape_instruction_port;

        sc_out<int> tape_address_port;

        sc_in<Symbols> tape_data_in_port;

        sc_out<Symbols> tape_data_out_port;

        sc_in<Tape_Statuses> tape_status_port;

    private:
        int index = 0;

        Rules rules;

        States state;

        Symbols symbol;

        Rule rule;

        Process_States process_state = Read_Memory;

        States halt_state;

        Symbols blank_symbol;

        Actions left_action;

        Actions stay_action;

        Actions right_action;

    public:
        Generic_Turing_Machine(
                const sc_module_name &name, States initial_state,
                States halt_state, Symbols blank_symbol,
                Actions left_action, Actions stay_action,
                Actions right_action, Rules rules) :
            sc_module(name) {

            SC_METHOD(process);

            this->dont_initialize();

            this->sensitive << this->clock_port.pos();

            this->state = initial_state;

            this->halt_state = halt_state;

            this->blank_symbol = blank_symbol;

            this->left_action = left_action;

            this->stay_action = stay_action;

            this->right_action = right_action;

            this->rules = rules;
        }

    private:
        void process() {
            if (this->state == this->halt_state) {
                sc_stop(); // This is a simulator method that will be stripped out by the
                           // synthesizer
            } else {
                if (this->process_state == Read_Memory &&
                    this->tape_status_port == Tape_Statuses::Read_Done) {
                    this->symbol = this->tape_data_in_port;

                    this->rule = this->rules[this->state][this->symbol];

                    this->tape_instruction_port = Tape_Instructions::Write;

                    this->tape_address_port = this->index;

                    this->tape_data_out_port = this->rule.symbol;

                    this->process_state = Write_Memory;
                } else if (this->process_state == Write_Memory &&
                           this->tape_status_port == Tape_Statuses::Write_Done) {
                    this->process_state = Finalize;

                    if (this->rule.action == this->left_action) {
                        if (this->index == 0) {
                            this->tape_instruction_port = Tape_Instructions::Shift;

                            this->tape_data_out_port = this->blank_symbol;

                            this->process_state = Shift_Memory;
                        } else {
                            this->index--;
                        }
                    } else if (this->rule.action == this->stay_action) {
                        // No-op
                    } else if (this->rule.action == this->right_action) {
                        this->index++;
                    }
                } else if (this->process_state == Shift_Memory &&
                           this->tape_status_port == Tape_Statuses::Shift_Done) {
                    this->process_state = Finalize;
                } else if (this->process_state == Finalize) {
                    this->tape_instruction_port = Tape_Instructions::Read;

                    this->tape_address_port = this->index;

                    this->process_state = Read_Memory;

                    this->state = this->rule.state;
                }
            }
        }
    };
} // namespace Universal_Turing_Machine

#endif // SYSTEMC_GENERIC_TURING_MACHINE_H