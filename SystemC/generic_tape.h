#ifndef SYSTEMC_GENERIC_TAPE_H
#define SYSTEMC_GENERIC_TAPE_H

#include "interface_tape.h"
#include <systemc/systemc.h>

namespace Universal_Turing_Machine {
    template<typename Symbols> class Generic_Tape : sc_module {
        SC_HAS_PROCESS(Generic_Tape);

    public:
        sc_in<bool> clock_port;

        sc_in<Tape_Instructions> instruction_port;

        sc_in<int> address_port;

        sc_in<Symbols> data_in_port;

        sc_out<Symbols> data_out_port;

        sc_out<Tape_Statuses> status_port;

        Symbols *buffer;

    private:
        int previous_address = -1;

        int size {};

        Tape_Instructions previous_instruction = Tape_Instructions::Read;

    public:
        Generic_Tape(const sc_module_name &name, int size) :
            sc_module(name) {
            SC_METHOD(process)

            this->dont_initialize();

            this->sensitive << this->clock_port.pos();

            this->buffer = new Symbols[size];

            this->size = size;
        }

    private:
        void process() {
            if (this->address_port != previous_address ||
                this->instruction_port != previous_instruction) {
                this->previous_address = this->address_port;

                this->previous_instruction = this->instruction_port;

                if (this->instruction_port == Tape_Instructions::Read) {
                    this->data_out_port = this->buffer[this->address_port];

                    this->status_port = Tape_Statuses::Read_Done;
                } else if (this->instruction_port == Tape_Instructions::Write) {
                    this->buffer[this->address_port] = this->data_in_port;

                    this->status_port = Tape_Statuses::Write_Done;
                } else if (instruction_port == Tape_Instructions::Shift) {
                    for (int i = this->size - 1; i >= 0; i--) {
                        this->buffer[i + 1] = this->buffer[i];
                    }

                    this->buffer[0] = this->data_in_port;

                    this->status_port = Tape_Statuses::Shift_Done;
                }
            }
        }
    };
} // namespace Universal_Turing_Machine

#endif // SYSTEMC_GENERIC_TAPE_H