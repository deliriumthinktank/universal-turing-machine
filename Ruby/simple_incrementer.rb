require './turing_machine'

rules = {
    :q0 => {1  => [1, :right, :q0],
            :b => [1, :stay,  :qf]}
}

tape = [1, 1, 1]

turing_machine = TuringMachine.new(:q0, :qf, :b, :left, :stay, :right)

turing_machine.start(tape, rules)

print tape


