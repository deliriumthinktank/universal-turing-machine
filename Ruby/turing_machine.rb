class TuringMachine
  def initialize(initial_state, terminating_state, blank_symbol, left_action_alias, stay_action_alias, right_action_alias)
    @initial_state = initial_state

    @terminating_state = terminating_state

    @blank_symbol = blank_symbol

    self.define_singleton_method(left_action_alias) do
      if @index == 0
        @tape.unshift @blank_symbol
      else
        @index -= 1
      end
    end

    self.define_singleton_method(stay_action_alias) do
    end

    self.define_singleton_method(right_action_alias) do
      @index += 1
    end
  end

  def start(tape, rules)
    state = @initial_state

    @tape = tape

    @index = 0

    while state != @terminating_state
      @tape[@index], action, state = rules[state][@tape[@index] ||= @blank_symbol]

      self.send action
    end
  end
end