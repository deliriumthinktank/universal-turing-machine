require './turing_machine'

rules = {
    :a => {0 => [1, :right, :b],
           1 => [1, :left,  :c]},
    :b => {0 => [1, :left,  :a],
           1 => [1, :right, :b]},
    :c => {0 => [1, :left,  :b],
           1 => [1, :stay,  :halt]}
}

tape = []

turing_machine = TuringMachine.new(:a, :halt, 0, :left, :stay, :right)

turing_machine.start(tape, rules)

print tape