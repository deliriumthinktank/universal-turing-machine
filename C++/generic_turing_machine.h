#ifndef C___GENERIC_TURING_MACHINE_H
#define C___GENERIC_TURING_MACHINE_H

#include <map>
#include <vector>

namespace Universal_Turing_Machine {
    template<typename States, typename Symbols, typename Actions> class Generic_Turing_Machine {
    public:
        typedef std::vector<Symbols> Tape;

        typedef struct {
            Symbols symbol;
            Actions action;
            States state;
        } Rule;

        typedef std::map<States, std::map<Symbols, Rule>> Rules;

    private:
        typedef int Index;

    private:
        Tape tape;

        Index index = 0;

        States state;

        States halt_state;

        Symbols blank_symbol;

        Actions left_action;

        Actions stay_action;

        Actions right_action;

    public:
        Generic_Turing_Machine<States, Symbols, Actions>(
                States initial_state, States halt_state, Symbols blank_symbol,
                Actions left_action, Actions stay_action, Actions right_action,
                Tape tape) {
            this->state = initial_state;

            this->halt_state = halt_state;

            this->blank_symbol = blank_symbol;

            this->left_action = left_action;

            this->stay_action = stay_action;

            this->right_action = right_action;

            this->tape = tape;
        }

        Tape execute(Rules rules) {
            if (this->tape.empty()) {
                this->tape.push_back(this->blank_symbol);
            }

            while (this->state != this->halt_state) {
                auto symbol = this->tape[this->index];

                auto rule = rules[this->state][symbol];

                this->tape[this->index] = rule.symbol;

                if (rule.action == this->left_action) {
                    if (this->index == 0) {
                        this->tape.insert(this->tape.begin(), this->blank_symbol);
                    } else {
                        this->index--;
                    }
                } else if (rule.action == this->stay_action) {
                    // Noop
                } else if (rule.action == this->right_action) {
                    if (this->index == this->tape.size() - 1) {
                        this->tape.push_back(this->blank_symbol);
                    }

                    this->index++;
                }

                this->state = rule.state;
            }

            return this->tape;
        }
    };
} // namespace Universal_Turing_Machine

#endif // C___GENERIC_TURING_MACHINE_H
