#include "generic_turing_machine.h"
#include <iostream>

using namespace std;
using namespace Universal_Turing_Machine;

int main() {
    enum States { A, B, C, D, E, H };

    typedef int Symbols;

    enum Actions { Left, Stay, Right };

    using Specific_Turing_Machine = Generic_Turing_Machine<States, Symbols, Actions>;

    Specific_Turing_Machine::Tape tape;

    Specific_Turing_Machine::Rules rules = {
        {A, {{0, {1, Right, B}}, {1, {1, Left, C}}}},
        {B, {{0, {1, Right, C}}, {1, {1, Right, B}}}},
        {C, {{0, {1, Right, D}}, {1, {0, Left, E}}}},
        {D, {{0, {1, Left, A}}, {1, {1, Left, D}}}},
        {E, {{0, {1, Stay, H}}, {1, {0, Left, A}}}},
    };

    auto specific_turing_machine =
            Specific_Turing_Machine(A, H, 0, Left, Stay, Right, tape);

    auto result = specific_turing_machine.execute(rules);

    for (Symbols symbol : result) {
        cout << symbol;
    }

    return 0;
}
