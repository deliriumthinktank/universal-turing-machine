#include "generic_turing_machine.h"
#include <iostream>

using namespace std;
using namespace Universal_Turing_Machine;

int main() {
    enum States { Q0, Qf };

    enum Symbols { B, One };

    enum Actions { Left, Stay, Right };

    using Specific_Turing_Machine = Generic_Turing_Machine<States, Symbols, Actions>;

    Specific_Turing_Machine::Tape tape {One, One, One, B};

    Specific_Turing_Machine::Rules rules = {
        {Q0, {{One, {One, Right, Q0}}, {B, {One, Stay, Qf}}}}};

    auto specific_turing_machine =
            Specific_Turing_Machine(Q0, Qf, B, Left, Stay, Right, tape);

    auto result = specific_turing_machine.execute(rules);

    for (Symbols symbol : result) {
        cout << symbol;
    }

    return 0;
}
