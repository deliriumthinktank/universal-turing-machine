# Universal Turing Machine in Ada 2012

The implementation of the universal turing machine in Ada 2012. The machine itself is implemented in `generic_turing_machine.*` and there are 3 examples that implement the tasks presented by [Rosetta Code](https://rosettacode.org/wiki/Universal_Turing_machine).
