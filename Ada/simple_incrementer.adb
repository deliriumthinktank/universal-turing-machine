with Text_IO, Ada.Containers.Vectors, Generic_Turing_Machine;

procedure Simple_Incrementer is
   type States_Type is (q0, qf);

   type Symbols_Type is (B, One);

   type Actions_Type is (Left, Right, Stay);

   package Specific_Tape_Vectors is new Ada.Containers.Vectors
     (Natural, Symbols_Type);

   package Specific_Turing_Machine is new Generic_Turing_Machine
     (States_Type           => States_Type,
      Symbols_Type          => Symbols_Type,
      Actions_Type          => Actions_Type,
      Specific_Tape_Vectors => Specific_Tape_Vectors);

   use Specific_Tape_Vectors;

   Tape : Vector := One & One & One;

   use Specific_Turing_Machine;

   Machine : Machine_Type :=
     (Initial_State => q0,
      Halt_State    => qf,
      Blank_Symbol  => B,
      Left_Action   => Left,
      Stay_Action   => Stay,
      Right_Action  => Right,
      Tape          => Tape);

   Rules : Rules_Type :=
     (q0 => (One => (q0, Right, One),
             B   => (qf, Stay,  One)));

   Result : Vector;
begin
   Result := Machine.Execute (Rules);

   for Symbol of Result loop
      Text_IO.Put (Symbols_Type'Image (Symbol) & " ");
   end loop;

   null;
end Simple_Incrementer;
