package body Generic_Turing_Machine is
   function Execute
     (Machine : in out Machine_Type; Rules : Rules_Type)
      return Specific_Tape_Vectors.Vector
   is
      State : States_Type := Machine.Initial_State;

      Symbol : Symbols_Type;

      Rule : Rule_Type;

      use Specific_Tape_Vectors;

      Index : Index_Type := 0;
   begin
      if Machine.Tape.Is_Empty then
         Machine.Tape.Append (Machine.Blank_Symbol);
      end if;

      while State /= Machine.Halt_State loop
         Symbol := Machine.Tape.Element (Index);

         Rule := Rules (State, Symbol);

         Machine.Tape.Replace_Element (Index, Rule.Symbol);

         if (Rule.Action = Machine.Left_Action) then
            if Index = 0 then
               Machine.Tape.Prepend (Machine.Blank_Symbol);
            else
               Index := Index - 1;
            end if;
         elsif (Rule.Action = Machine.Stay_Action) then
            null;
         elsif (Rule.Action = Machine.Right_Action) then
            if Index = Machine.Tape.Last_Index then
               Machine.Tape.Append (Machine.Blank_Symbol);
            end if;

            Index := Index + 1;
         end if;

         State := Rule.State;
      end loop;

      return Machine.Tape;
   end Execute;
end Generic_Turing_Machine;
