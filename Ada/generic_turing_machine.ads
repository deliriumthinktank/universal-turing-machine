with Ada.Containers.Vectors;

generic
   type States_Type is (<>);
   type Symbols_Type is (<>);
   type Actions_Type is (<>);
   with package Specific_Tape_Vectors is new Ada.Containers.Vectors
     (Element_Type => Symbols_Type, others => <>);
package Generic_Turing_Machine is
   type Rule_Type is record
      State  : States_Type;
      Action : Actions_Type;
      Symbol : Symbols_Type;
   end record;

   type Rules_Type is
     array (States_Type range <>, Symbols_Type range <>) of Rule_Type;

   type Machine_Type is tagged record
      Initial_State : States_Type;
      Halt_State    : States_Type;
      Blank_Symbol  : Symbols_Type;
      Left_Action   : Actions_Type;
      Stay_Action   : Actions_Type;
      Right_Action  : Actions_Type;
      Tape          : Specific_Tape_Vectors.Vector;
   end record;

   function Execute
     (Machine : in out Machine_Type; Rules : Rules_Type)
      return Specific_Tape_Vectors.Vector;
end Generic_Turing_Machine;
