with Text_IO, Ada.Containers.Vectors, Generic_Turing_Machine;

procedure Three_State_Busy_Beaver is
   type States_Type is (A, B, C, Stop);

   type Symbols_Type is range 0 .. 1;

   type Actions_Type is (Left, Right, Stay);

   package Specific_Tape_Vectors is new Ada.Containers.Vectors
     (Natural, Symbols_Type);

   package Specific_Turing_Machine is new Generic_Turing_Machine
     (States_Type           => States_Type,
      Symbols_Type          => Symbols_Type,
      Actions_Type          => Actions_Type,
      Specific_Tape_Vectors => Specific_Tape_Vectors);

   use Specific_Tape_Vectors;

   Tape : Vector;

   use Specific_Turing_Machine;

   Machine : Machine_Type :=
     (Initial_State => A,
      Halt_State    => Stop,
      Blank_Symbol  => 0,
      Left_Action   => Left,
      Stay_Action   => Stay,
      Right_Action  => Right,
      Tape          => Tape);

   Rules : Rules_Type :=
     (A => (0 => (B,    Right, 1),
            1 => (C,    Left,  1)),
      B => (0 => (A,    Left,  1),
            1 => (B,    Right, 1)),
      C => (0 => (B,    Left,  1),
            1 => (Stop, Stay,  1)));

   Result : Vector;
begin
   Result := Machine.Execute (Rules);

   for Symbol of Result loop
      Text_IO.Put (Symbols_Type'Image (Symbol));
   end loop;

   null;
end Three_State_Busy_Beaver;
